package com.finplat.test.services.student.impl;

import com.finplat.test.models.Student;
import com.finplat.test.repositories.StudentRepo;
import com.finplat.test.services.AbstractServiceImpl;
import com.finplat.test.services.student.StudentService;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class StudentServiceImpl extends AbstractServiceImpl<Student, Long> implements StudentService {

    private final StudentRepo studentRepo;

    protected StudentServiceImpl(CrudRepository<Student, Long> crudRepository, StudentRepo studentRepo) {
        super(crudRepository);
        this.studentRepo = studentRepo;
    }

    @Override
    public Student findByUin(Long uin) {
        return studentRepo.findStudentByUIN(uin);
    }

    @Override
    public boolean existsStudentByUIN(Integer uin) {
        return studentRepo.existsStudentByUIN(uin);
    }

    @Override
    @Transactional
    public Long deleteStudentByUIN(Integer uin){
        return studentRepo.deleteByUIN(uin);
    }
}
