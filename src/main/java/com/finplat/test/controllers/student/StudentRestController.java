package com.finplat.test.controllers.student;

import com.finplat.test.dto.StudentDto;
import com.finplat.test.dto.converter.StudentDtoConverter;
import com.finplat.test.models.Student;
import com.finplat.test.services.student.StudentService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RestController
@RequestMapping("/api/student")
@RequiredArgsConstructor
public class StudentRestController {

    private final StudentService studentService;
    private final StudentDtoConverter studentDtoConverter;

    @Operation(description = "Получение списка всех студентов")
    @GetMapping()
    public ResponseEntity<List<StudentDto>> getStudents() {
        return ResponseEntity.ok(studentDtoConverter.toDtoList((List<Student>)studentService.findAll()));
    }

    @Operation(description = "Создание студента")
    @ApiResponse(responseCode = "200", description = "Студент успешно создан")
    @ApiResponse(responseCode = "400", description = "Ошибка создания студента")
    @PostMapping()
    private ResponseEntity<StudentDto> saveStudent(@RequestBody StudentDto studentDto){
        Student student = studentDtoConverter.toEntity(studentDto);
        student.setId(null);
        studentService.save(student);
        return ResponseEntity.ok(studentDtoConverter.toDto(student));
    }

    @Operation(description = "Удаление студента по уникальному номеру UIN")
    @ApiResponse(responseCode = "200", description = "Студент успешно удален")
    @ApiResponse(responseCode = "400", description = "Ошибка удаления студента")
    @DeleteMapping("/{uin}")
    public ResponseEntity<?> deleteStudent(@PathVariable(value = "uin") Integer uin) {
        if (!studentService.existsStudentByUIN(uin)){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Студент с uid = " + uin + " в базе не найден");
        }
        studentService.deleteStudentByUIN(uin);
        return ResponseEntity.ok("Студент с uid = " + uin + " успешно удален");
    }
}
