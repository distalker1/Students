package com.finplat.test.services;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

public abstract class AbstractServiceImpl<T, ID> implements AbstractService<T, ID> {

    private final CrudRepository<T, ID> crudRepository;

    protected AbstractServiceImpl(CrudRepository<T, ID> crudRepository) {
        this.crudRepository = crudRepository;
    }

    @Override
    @Transactional
    public <S extends T> S save(S entity) {
        return crudRepository.save(entity);
    }

    @Override
    @Transactional
    public <S extends T> Iterable<S> saveAll(Iterable<S> entities) {
        return crudRepository.saveAll(entities);
    }

    @Override
    public Optional<T> findById(ID id) {
        return crudRepository.findById(id);
    }

    @Override
    public boolean existsById(ID id) {
        return crudRepository.existsById(id);
    }

    @Override
    public Iterable<T> findAll() {
        return crudRepository.findAll();
    }

    @Override
    public Iterable<T> findAllById(Iterable<ID> ids) {
        return crudRepository.findAllById(ids);
    }

    @Override
    public long count() {
        return crudRepository.count();
    }

    @Override
    @Transactional
    public void deleteById(ID id) {
        crudRepository.deleteById(id);
    }

    @Override
    @Transactional
    public void delete(T entity) {
        crudRepository.delete(entity);
    }

    @Override
    @Transactional
    public void deleteAllById(Iterable<? extends ID> ids) {
        crudRepository.deleteAllById(ids);
    }

    @Override
    @Transactional
    public void deleteAll(Iterable<? extends T> entities) {
        crudRepository.deleteAll(entities);
    }

    @Override
    @Transactional
    public void deleteAll() {
        crudRepository.deleteAll();
    }
}
