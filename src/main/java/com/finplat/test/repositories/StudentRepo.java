package com.finplat.test.repositories;

import com.finplat.test.models.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepo extends JpaRepository<Student, Long> {

    Student findStudentByUIN(Long uin);

    boolean existsStudentByUIN(Integer uin);

    Long deleteByUIN(Integer uin);
}
