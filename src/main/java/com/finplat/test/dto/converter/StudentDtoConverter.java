package com.finplat.test.dto.converter;

import com.finplat.test.dto.StudentDto;
import com.finplat.test.models.Student;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class StudentDtoConverter {

    public StudentDto toDto(Student student){
        return StudentDto.builder()
                .id(student.getId())
                .name(student.getName())
                .lastname(student.getLastname())
                .patronymic(student.getPatronymic())
                .birthday(student.getBirthday())
                .numberOfGroup(student.getNumberOfGroup())
                .uin(student.getUIN())
                .build();
    }

    public Student toEntity(StudentDto studentDto){
        return Student.builder()
                .id(studentDto.getId())
                .name(studentDto.getName())
                .lastname(studentDto.getLastname())
                .patronymic(studentDto.getPatronymic())
                .birthday(studentDto.getBirthday())
                .numberOfGroup(studentDto.getNumberOfGroup())
                .UIN(studentDto.getUin())
                .build();
    }

    public List<StudentDto> toDtoList(List<Student> students){
        List<StudentDto> studentDtos = new ArrayList<>();
        for (Student student : students) {
            studentDtos.add(toDto(student));
        }
        return studentDtos;
    }

    public List<Student> toEntityList(List<StudentDto> studentDtos){
        List<Student> students = new ArrayList<>();
        for (StudentDto studentDto : studentDtos) {
            students.add(toEntity(studentDto));
        }
        return students;
    }

}
