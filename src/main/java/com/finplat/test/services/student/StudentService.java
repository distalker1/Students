package com.finplat.test.services.student;

import com.finplat.test.models.Student;
import com.finplat.test.services.AbstractService;

public interface StudentService extends AbstractService<Student, Long> {

    Student findByUin(Long uin);

    boolean existsStudentByUIN(Integer uin);

    Long deleteStudentByUIN(Integer uin);

}
